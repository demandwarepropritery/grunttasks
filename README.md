# Grunt Tasks #

This repo contains the Grunt tasks that I've written to support Demandware development.

* ### Error Log Analyzer ###

* ### Code Version Cleaner ###

* ### Reset Password ###

**NOTE:** Demandware recently updated their BM interface so I don't think the **Reset Password** script will work anymore. Demandware also states that using a script like this to reset a BM password is a violation of PCI DSS, so you should take that into consideration.