'use strict';

module.exports = function(grunt) {
    grunt.registerTask('resetpassword', 'Resets a password on a Demandware instance', function() {
        var options = grunt.config('resetpassword.options');

        //NOTE: async() is a Grunt function that causes the main control thread to look for 
        //  the corresponding execution of the done() function to be called before
        //  exiting the current function - allows for this code to wait for an asynchronous 
        //  command to complete - otherwise this code would complete too soon and no work would
        //  be done.  If done() is not eventually invoked then the code will 'hang'.
        
        var done = this.async();  //Grunt synchronization
        
        resetPassword({
            host: options.webdav_server, 
            username: options.webdav_username, 
            password: options.webdav_password
        }, done);         
    }); 
}

/*
Author: Jorge Matos
Date: 9/11/2014

Description: This function resets a BusinessManager password by using the Zombie Node library to automate 
                a headless browser.  It changes the password 5 times to a random password and then resets
                the password to the original password. 

        
Dependencies:
    Zombie JS v2.5

Usage: 
    grunt resetpassword -WEBDAV_SERVER='my server hostname' -WEBDAV_USERNAME='my BM username' -WEBDAV_PASSWORD='my BM password'
   
*/

function resetPassword(options, callback) {
    var host = options.host;
    var username = options.username;
    var password = options.password;

    var ZombieBrowser = require('zombie');
    var origPassword = password;
    var newPassword = getTempPWD();
    var url = 'https://' + host + '/on/demandware.store/Sites-Site';
    var browser = ZombieBrowser.create({debug: false, runScripts: false, maxWait: 10000, waitFor: 10000});
    
    return browser
        .visit(url)
        .then(function () {
                console.log('Logging into BM: ' + host + ' - ' + username);                     
                return browser
                        .fill('LoginForm_Login', username)
                        .fill('LoginForm_Password', password)
                        .pressButton('login');
        })
        .then(function () {
            console.log('Clicking profile link');
            return browser.clickLink("a[title='User Profile']");    
        })
        .then(function () {
            console.log('Clicking Change password and security settings link');
            return browser.clickLink('a.action_link');  
        })  
        .then(function () {
            return changePassword(browser, password, newPassword);            
        })
        .then(function () {
            password = newPassword;
            newPassword = getTempPWD();
            return changePassword(browser, password, newPassword);
        })
        .then(function () {
            password = newPassword;
            newPassword = getTempPWD();
            return changePassword(browser, password, newPassword);
        })
        .then(function () {
            password = newPassword;
            newPassword = getTempPWD();
            return changePassword(browser, password, newPassword);
        })
        .then(function () {
            password = newPassword;
            newPassword = getTempPWD();
            return changePassword(browser, password, newPassword);
        })
        .then(function () {
            password = newPassword;
            newPassword = origPassword;
            return changePassword(browser, password, newPassword);
        })
        .then(function () {
            console.log('Logging out of BM');
            return browser.clickLink("a[title='Log off.']");
        })
        .done(function() { 
            console.log('All Done');  
            callback();     
        });
}

function changePassword(browser, password, newPassword) {
    console.log('Changing password: [old, new]: [' + password + ', ' + newPassword + ']');
    return browser
            .fill('#CredentialsYourPassword', password)
            .fill('#CredentialsPassword', newPassword)
            .fill('#CredentialsPasswordConfirm', newPassword)
            .pressButton("button[name='updateSecuritySettings']");
}

//Return temp password with 5 characters and 3 numbers
function getTempPWD() {
    var letters = ['a','b','c','d','e','f','g','h','i','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    var numbers = [0,1,2,3,4,5,6,7,8,9];
    var randomstring = '';

    for(var i=0;i<5;i++){
        var rlet = Math.floor(Math.random()*letters.length);
        randomstring += letters[rlet];
    }
    for(var i=0;i<3;i++){
        var rnum = Math.floor(Math.random()*numbers.length);
        randomstring += numbers[rnum];
    }
    return randomstring;
}